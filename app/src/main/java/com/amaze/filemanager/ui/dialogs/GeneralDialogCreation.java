/*
 * Copyright (C) 2014-2020 Arpit Khurana <arpitkh96@gmail.com>, Vishal Nehra <vishalmeham2@gmail.com>,
 * Emmanuel Messulam<emmanuelbendavid@gmail.com>, Raymond Lai <airwave209gt at gmail.com> and Contributors.
 *
 * This file is part of Amaze File Manager.
 *
 * Amaze File Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.amaze.filemanager.ui.dialogs;

import static android.os.Build.VERSION_CODES.M;
import static com.amaze.filemanager.filesystem.files.FileUtils.toHybridFileArrayList;
import static com.amaze.filemanager.ui.fragments.preference_fragments.PreferencesConstants.PREFERENCE_SORTBY_ONLY_THIS;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.afollestad.materialdialogs.internal.MDButton;
import com.amaze.filemanager.R;
import com.amaze.filemanager.adapters.HiddenAdapter;
import com.amaze.filemanager.adapters.data.LayoutElementParcelable;
import com.amaze.filemanager.application.AppConfig;
import com.amaze.filemanager.asynchronous.asynctasks.CountItemsOrAndSizeTask;
import com.amaze.filemanager.asynchronous.asynctasks.GenerateHashesTask;
import com.amaze.filemanager.asynchronous.asynctasks.LoadFolderSpaceDataTask;
import com.amaze.filemanager.asynchronous.services.EncryptService;
import com.amaze.filemanager.database.SortHandler;
import com.amaze.filemanager.database.models.explorer.Sort;
import com.amaze.filemanager.exceptions.ShellNotRunningException;
import com.amaze.filemanager.filesystem.FileUtil;
import com.amaze.filemanager.filesystem.HybridFile;
import com.amaze.filemanager.filesystem.HybridFileParcelable;
import com.amaze.filemanager.filesystem.files.CryptUtil;
import com.amaze.filemanager.filesystem.files.EncryptDecryptUtils;
import com.amaze.filemanager.filesystem.files.FileUtils;
import com.amaze.filemanager.ui.activities.MainActivity;
import com.amaze.filemanager.ui.activities.superclasses.ThemedActivity;
import com.amaze.filemanager.ui.fragments.AppsListFragment;
import com.amaze.filemanager.ui.fragments.MainFragment;
import com.amaze.filemanager.ui.fragments.preference_fragments.PreferencesConstants;
import com.amaze.filemanager.ui.theme.AppTheme;
import com.amaze.filemanager.ui.views.WarnableTextInputLayout;
import com.amaze.filemanager.ui.views.WarnableTextInputValidator;
import com.amaze.filemanager.utils.DataUtils;
import com.amaze.filemanager.utils.FingerprintHandler;
import com.amaze.filemanager.utils.OpenMode;
import com.amaze.filemanager.utils.RootUtils;
import com.amaze.filemanager.utils.SimpleTextWatcher;
import com.amaze.filemanager.utils.Utils;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.material.textfield.TextInputEditText;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.AppCompatButton;

/**
 * Here are a lot of function that create material dialogs
 *
 * @author Emmanuel on 17/5/2017, at 13:27.
 */
public class GeneralDialogCreation {
  private static final String TAG = "GeneralDialogCreation";

  public static MaterialDialog showBasicDialog(
      ThemedActivity themedActivity,
      @StringRes int content,
      @StringRes int title,
      @StringRes int postiveText,
      @StringRes int negativeText) {
    int accentColor = themedActivity.getAccent();
    MaterialDialog.Builder a =
        new MaterialDialog.Builder(themedActivity)
            .content(content)
            .widgetColor(accentColor)
            .theme(themedActivity.getAppTheme().getMaterialDialogTheme())
            .title(title)
            .positiveText(postiveText)
            .positiveColor(accentColor)
            .negativeText(negativeText)
            .negativeColor(accentColor);
    return a.build();
  }

  public static MaterialDialog showNameDialog(
      final MainActivity m,
      String hint,
      String prefill,
      String title,
      String positiveButtonText,
      String neutralButtonText,
      String negativeButtonText,
      MaterialDialog.SingleButtonCallback positiveButtonAction,
      WarnableTextInputValidator.OnTextValidate validator) {
    int accentColor = m.getAccent();
    MaterialDialog.Builder builder = new MaterialDialog.Builder(m);

    View dialogView = m.getLayoutInflater().inflate(R.layout.dialog_singleedittext, null);
    EditText textfield = dialogView.findViewById(R.id.singleedittext_input);
    textfield.setHint(hint);
    textfield.setText(prefill);

    WarnableTextInputLayout tilTextfield =
        dialogView.findViewById(R.id.singleedittext_warnabletextinputlayout);

    builder
        .customView(dialogView, false)
        .widgetColor(accentColor)
        .theme(m.getAppTheme().getMaterialDialogTheme())
        .title(title)
        .positiveText(positiveButtonText)
        .onPositive(positiveButtonAction);

    if (neutralButtonText != null) {
      builder.neutralText(neutralButtonText);
    }

    if (negativeButtonText != null) {
      builder.negativeText(negativeButtonText);
      builder.negativeColor(accentColor);
    }

    MaterialDialog dialog = builder.show();

    WarnableTextInputValidator textInputValidator =
        new WarnableTextInputValidator(
            builder.getContext(),
            textfield,
            tilTextfield,
            dialog.getActionButton(DialogAction.POSITIVE),
            validator);

    if (!TextUtils.isEmpty(prefill)) textInputValidator.afterTextChanged(textfield.getText());

    return dialog;
  }

  @SuppressWarnings("ConstantConditions")
  public static void deleteFilesDialog(
      final Context c,
      final ArrayList<LayoutElementParcelable> layoutElements,
      final MainActivity mainActivity,
      final List<LayoutElementParcelable> positions,
      AppTheme appTheme) {

    final ArrayList<HybridFileParcelable> itemsToDelete = new ArrayList<>();
    int accentColor = mainActivity.getAccent();

    // Build dialog with custom view layout and accent color.
    MaterialDialog dialog =
        new MaterialDialog.Builder(c)
            .title(c.getString(R.string.dialog_delete_title))
            .customView(R.layout.dialog_delete, true)
            .theme(appTheme.getMaterialDialogTheme())
            .negativeText(c.getString(R.string.cancel).toUpperCase())
            .positiveText(c.getString(R.string.delete).toUpperCase())
            .positiveColor(accentColor)
            .negativeColor(accentColor)
            .onPositive(
                (dialog1, which) -> {
                  Toast.makeText(c, c.getString(R.string.deleting), Toast.LENGTH_SHORT).show();
                  mainActivity.mainActivityHelper.deleteFiles(itemsToDelete);
                })
            .build();

    // Get views from custom layout to set text values.
    final TextView categoryDirectories =
        dialog.getCustomView().findViewById(R.id.category_directories);
    final TextView categoryFiles = dialog.getCustomView().findViewById(R.id.category_files);
    final TextView listDirectories = dialog.getCustomView().findViewById(R.id.list_directories);
    final TextView listFiles = dialog.getCustomView().findViewById(R.id.list_files);
    final TextView total = dialog.getCustomView().findViewById(R.id.total);

    // Parse items to delete.

    new AsyncTask<Void, Object, Void>() {

      long sizeTotal = 0;
      StringBuilder files = new StringBuilder();
      StringBuilder directories = new StringBuilder();
      int counterDirectories = 0;
      int counterFiles = 0;

      @Override
      protected void onPreExecute() {
        super.onPreExecute();

        listFiles.setText(c.getString(R.string.loading));
        listDirectories.setText(c.getString(R.string.loading));
        total.setText(c.getString(R.string.loading));
      }

      @Override
      protected Void doInBackground(Void... params) {

        for (int i = 0; i < positions.size(); i++) {
          final LayoutElementParcelable layoutElement = positions.get(i);
          itemsToDelete.add(layoutElement.generateBaseFile());

          // Build list of directories to delete.
          if (layoutElement.isDirectory) {
            // Don't add newline between category and list.
            if (counterDirectories != 0) {
              directories.append("\n");
            }

            long sizeDirectory = layoutElement.generateBaseFile().folderSize(c);

            directories
                .append(++counterDirectories)
                .append(". ")
                .append(layoutElement.title)
                .append(" (")
                .append(Formatter.formatFileSize(c, sizeDirectory))
                .append(")");
            sizeTotal += sizeDirectory;
            // Build list of files to delete.
          } else {
            // Don't add newline between category and list.
            if (counterFiles != 0) {
              files.append("\n");
            }

            files
                .append(++counterFiles)
                .append(". ")
                .append(layoutElement.title)
                .append(" (")
                .append(layoutElement.size)
                .append(")");
            sizeTotal += layoutElement.longSize;
          }

          publishProgress(sizeTotal, counterFiles, counterDirectories, files, directories);
        }
        return null;
      }

      @Override
      protected void onProgressUpdate(Object... result) {
        super.onProgressUpdate(result);

        int tempCounterFiles = (int) result[1];
        int tempCounterDirectories = (int) result[2];
        long tempSizeTotal = (long) result[0];
        StringBuilder tempFilesStringBuilder = (StringBuilder) result[3];
        StringBuilder tempDirectoriesStringBuilder = (StringBuilder) result[4];

        updateViews(
            tempSizeTotal,
            tempFilesStringBuilder,
            tempDirectoriesStringBuilder,
            tempCounterFiles,
            tempCounterDirectories);
      }

      @Override
      protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        updateViews(sizeTotal, files, directories, counterFiles, counterDirectories);
      }

      private void updateViews(
          long tempSizeTotal,
          StringBuilder filesStringBuilder,
          StringBuilder directoriesStringBuilder,
          int... values) {

        int tempCounterFiles = values[0];
        int tempCounterDirectories = values[1];

        // Hide category and list for directories when zero.
        if (tempCounterDirectories == 0) {

          if (tempCounterDirectories == 0) {

            categoryDirectories.setVisibility(View.GONE);
            listDirectories.setVisibility(View.GONE);
          }
          // Hide category and list for files when zero.
        }

        if (tempCounterFiles == 0) {

          categoryFiles.setVisibility(View.GONE);
          listFiles.setVisibility(View.GONE);
        }

        if (tempCounterDirectories != 0 || tempCounterFiles != 0) {
          listDirectories.setText(directoriesStringBuilder);
          if (listDirectories.getVisibility() != View.VISIBLE && tempCounterDirectories != 0)
            listDirectories.setVisibility(View.VISIBLE);
          listFiles.setText(filesStringBuilder);
          if (listFiles.getVisibility() != View.VISIBLE && tempCounterFiles != 0)
            listFiles.setVisibility(View.VISIBLE);

          if (categoryDirectories.getVisibility() != View.VISIBLE && tempCounterDirectories != 0)
            categoryDirectories.setVisibility(View.VISIBLE);
          if (categoryFiles.getVisibility() != View.VISIBLE && tempCounterFiles != 0)
            categoryFiles.setVisibility(View.VISIBLE);
        }

        // Show total size with at least one directory or file and size is not zero.
        if (tempCounterFiles + tempCounterDirectories > 1 && tempSizeTotal > 0) {
          StringBuilder builderTotal =
              new StringBuilder()
                  .append(c.getString(R.string.total))
                  .append(" ")
                  .append(Formatter.formatFileSize(c, tempSizeTotal));
          total.setText(builderTotal);
          if (total.getVisibility() != View.VISIBLE) total.setVisibility(View.VISIBLE);
        } else {
          total.setVisibility(View.GONE);
        }
      }
    }.execute();

    // Set category text color for Jelly Bean (API 16) and later.
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      categoryDirectories.setTextColor(accentColor);
      categoryFiles.setTextColor(accentColor);
    }

    // Show dialog on screen.
    dialog.show();
  }

  public static void showPropertiesDialogWithPermissions(
      HybridFileParcelable baseFile,
      final String permissions,
      ThemedActivity activity,
      boolean isRoot,
      AppTheme appTheme) {
    showPropertiesDialog(baseFile, permissions, activity, isRoot, appTheme, true, false);
  }

  public static void showPropertiesDialogWithoutPermissions(
      final HybridFileParcelable f, ThemedActivity activity, AppTheme appTheme) {
    showPropertiesDialog(f, null, activity, false, appTheme, false, false);
  }

  public static void showPropertiesDialogForStorage(
      final HybridFileParcelable f, ThemedActivity activity, AppTheme appTheme) {
    showPropertiesDialog(f, null, activity, false, appTheme, false, true);
  }

  private static void showPropertiesDialog(
      final HybridFileParcelable baseFile,
      final String permissions,
      ThemedActivity base,
      boolean isRoot,
      AppTheme appTheme,
      boolean showPermissions,
      boolean forStorage) {
    final ExecutorService executor = Executors.newFixedThreadPool(3);
    final Context c = base.getApplicationContext();
    int accentColor = base.getAccent();
    long last = baseFile.getDate();
    final String date = Utils.getDate(base, last),
        items = c.getString(R.string.calculating),
        name = baseFile.getName(c),
        parent = baseFile.getReadablePath(baseFile.getParent(c));

    File nomediaFile =
        baseFile.isDirectory() ? new File(baseFile.getPath() + "/" + FileUtils.NOMEDIA_FILE) : null;

    MaterialDialog.Builder builder = new MaterialDialog.Builder(base);
    builder.title(c.getString(R.string.properties));
    builder.theme(appTheme.getMaterialDialogTheme());

    View v = base.getLayoutInflater().inflate(R.layout.properties_dialog, null);
    TextView itemsText = v.findViewById(R.id.t7);
    CheckBox nomediaCheckBox = v.findViewById(R.id.nomediacheckbox);

    /*View setup*/
    {
      TextView mNameTitle = v.findViewById(R.id.title_name);
      mNameTitle.setTextColor(accentColor);

      TextView mDateTitle = v.findViewById(R.id.title_date);
      mDateTitle.setTextColor(accentColor);

      TextView mSizeTitle = v.findViewById(R.id.title_size);
      mSizeTitle.setTextColor(accentColor);

      TextView mLocationTitle = v.findViewById(R.id.title_location);
      mLocationTitle.setTextColor(accentColor);

      TextView md5Title = v.findViewById(R.id.title_md5);
      md5Title.setTextColor(accentColor);

      TextView sha256Title = v.findViewById(R.id.title_sha256);
      sha256Title.setTextColor(accentColor);

      ((TextView) v.findViewById(R.id.t5)).setText(name);
      ((TextView) v.findViewById(R.id.t6)).setText(parent);
      itemsText.setText(items);
      ((TextView) v.findViewById(R.id.t8)).setText(date);

      if (baseFile.isDirectory() && baseFile.isLocal()) {
        nomediaCheckBox.setVisibility(View.VISIBLE);
        if (nomediaFile != null) {
          nomediaCheckBox.setChecked(nomediaFile.exists());
        }
      }

      LinearLayout mNameLinearLayout = v.findViewById(R.id.properties_dialog_name);
      LinearLayout mLocationLinearLayout = v.findViewById(R.id.properties_dialog_location);
      LinearLayout mSizeLinearLayout = v.findViewById(R.id.properties_dialog_size);
      LinearLayout mDateLinearLayout = v.findViewById(R.id.properties_dialog_date);

      // setting click listeners for long press
      mNameLinearLayout.setOnLongClickListener(
          v1 -> {
            FileUtils.copyToClipboard(c, name);
            Toast.makeText(
                    c,
                    c.getString(R.string.name)
                        + " "
                        + c.getString(R.string.properties_copied_clipboard),
                    Toast.LENGTH_SHORT)
                .show();
            return false;
          });
      mLocationLinearLayout.setOnLongClickListener(
          v12 -> {
            FileUtils.copyToClipboard(c, parent);
            Toast.makeText(
                    c,
                    c.getString(R.string.location)
                        + " "
                        + c.getString(R.string.properties_copied_clipboard),
                    Toast.LENGTH_SHORT)
                .show();
            return false;
          });
      mSizeLinearLayout.setOnLongClickListener(
          v13 -> {
            FileUtils.copyToClipboard(c, items);
            Toast.makeText(
                    c,
                    c.getString(R.string.size)
                        + " "
                        + c.getString(R.string.properties_copied_clipboard),
                    Toast.LENGTH_SHORT)
                .show();
            return false;
          });
      mDateLinearLayout.setOnLongClickListener(
          v14 -> {
            FileUtils.copyToClipboard(c, date);
            Toast.makeText(
                    c,
                    c.getString(R.string.date)
                        + " "
                        + c.getString(R.string.properties_copied_clipboard),
                    Toast.LENGTH_SHORT)
                .show();
            return false;
          });
    }

    CountItemsOrAndSizeTask countItemsOrAndSizeTask =
        new CountItemsOrAndSizeTask(c, itemsText, baseFile, forStorage);
    countItemsOrAndSizeTask.executeOnExecutor(executor);

    GenerateHashesTask hashGen = new GenerateHashesTask(baseFile, c, v);
    hashGen.executeOnExecutor(executor);

    /*Chart creation and data loading*/
    {
      boolean isRightToLeft = c.getResources().getBoolean(R.bool.is_right_to_left);
      boolean isDarkTheme = appTheme.getMaterialDialogTheme() == Theme.DARK;
      PieChart chart = v.findViewById(R.id.chart);

      chart.setTouchEnabled(false);
      chart.setDrawEntryLabels(false);
      chart.setDescription(null);
      chart.setNoDataText(c.getString(R.string.loading));
      chart.setRotationAngle(!isRightToLeft ? 0f : 180f);
      chart.setHoleColor(Color.TRANSPARENT);
      chart.setCenterTextColor(isDarkTheme ? Color.WHITE : Color.BLACK);

      chart.getLegend().setEnabled(true);
      chart.getLegend().setForm(Legend.LegendForm.CIRCLE);
      chart.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
      chart.getLegend().setTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL));
      chart.getLegend().setTextColor(isDarkTheme ? Color.WHITE : Color.BLACK);

      chart.animateY(1000);

      if (forStorage) {
        final String[] LEGENDS =
            new String[] {c.getString(R.string.used), c.getString(R.string.free)};
        final int[] COLORS = {
          Utils.getColor(c, R.color.piechart_red), Utils.getColor(c, R.color.piechart_green)
        };

        long totalSpace = baseFile.getTotal(c),
            freeSpace = baseFile.getUsableSpace(),
            usedSpace = totalSpace - freeSpace;

        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(usedSpace, LEGENDS[0]));
        entries.add(new PieEntry(freeSpace, LEGENDS[1]));

        PieDataSet set = new PieDataSet(entries, null);
        set.setColors(COLORS);
        set.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        set.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        set.setSliceSpace(5f);
        set.setAutomaticallyDisableSliceSpacing(true);
        set.setValueLinePart2Length(1.05f);
        set.setSelectionShift(0f);

        PieData pieData = new PieData(set);
        pieData.setValueFormatter(new SizeFormatter(c));
        pieData.setValueTextColor(isDarkTheme ? Color.WHITE : Color.BLACK);

        String totalSpaceFormatted = Formatter.formatFileSize(c, totalSpace);

        chart.setCenterText(
            new SpannableString(c.getString(R.string.total) + "\n" + totalSpaceFormatted));
        chart.setData(pieData);
      } else {
        LoadFolderSpaceDataTask loadFolderSpaceDataTask =
            new LoadFolderSpaceDataTask(c, appTheme, chart, baseFile);
        loadFolderSpaceDataTask.executeOnExecutor(executor);
      }

      chart.invalidate();
    }

    if (!forStorage && showPermissions) {
      final MainFragment main = ((MainActivity) base).getCurrentMainFragment();
      AppCompatButton appCompatButton = v.findViewById(R.id.permissionsButton);
      appCompatButton.setAllCaps(true);

      final View permissionsTable = v.findViewById(R.id.permtable);
      final View button = v.findViewById(R.id.set);
      if (isRoot && permissions.length() > 6) {
        appCompatButton.setVisibility(View.VISIBLE);
        appCompatButton.setOnClickListener(
            v15 -> {
              if (permissionsTable.getVisibility() == View.GONE) {
                permissionsTable.setVisibility(View.VISIBLE);
                button.setVisibility(View.VISIBLE);
                setPermissionsDialog(permissionsTable, button, baseFile, permissions, c, main);
              } else {
                button.setVisibility(View.GONE);
                permissionsTable.setVisibility(View.GONE);
              }
            });
      }
    }

    builder.customView(v, true);
    builder.positiveText(base.getString(R.string.ok));
    builder.positiveColor(accentColor);
    builder.dismissListener(dialog -> executor.shutdown());
    builder.onPositive(
        (dialog, which) -> {
          if (baseFile.isDirectory() && nomediaFile != null) {
            if (nomediaCheckBox.isChecked()) {
              // checkbox is checked, create .nomedia
              try {
                if (!nomediaFile.createNewFile()) {
                  // failed operation
                  Log.w(TAG, "'.nomedia' file creation in " + baseFile.getPath() + " failed!");
                }
              } catch (IOException e) {
                e.printStackTrace();
              }
            } else {
              // checkbox is unchecked, delete .nomedia
              if (!nomediaFile.delete()) {
                // failed operation
                Log.w(TAG, "'.nomedia' file deletion in " + baseFile.getPath() + " failed!");
              }
            }
          }
        });

    MaterialDialog materialDialog = builder.build();
    materialDialog.show();
    materialDialog.getActionButton(DialogAction.NEGATIVE).setEnabled(false);

    /*
    View bottomSheet = c.findViewById(R.id.design_bottom_sheet);
    BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    bottomSheetBehavior.setPeekHeight(BottomSheetBehavior.STATE_DRAGGING);
    */
  }

  public static class SizeFormatter implements IValueFormatter {

    private Context context;

    public SizeFormatter(Context c) {
      context = c;
    }

    @Override
    public String getFormattedValue(
        float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
      String prefix =
          entry.getData() != null && entry.getData() instanceof String
              ? (String) entry.getData()
              : "";

      return prefix + Formatter.formatFileSize(context, (long) value);
    }
  }


  public static void showEncryptWarningDialog(
      final Intent intent,
      final MainFragment main,
      AppTheme appTheme,
      final EncryptDecryptUtils.EncryptButtonCallbackInterface encryptButtonCallbackInterface) {
    int accentColor = main.getMainActivity().getAccent();
    final SharedPreferences preferences =
        PreferenceManager.getDefaultSharedPreferences(main.getContext());
    final MaterialDialog.Builder builder = new MaterialDialog.Builder(main.getActivity());
    builder.title(main.getString(R.string.warning));
    builder.content(main.getString(R.string.crypt_warning_key));
    builder.theme(appTheme.getMaterialDialogTheme());
    builder.negativeText(main.getString(R.string.warning_never_show));
    builder.positiveText(main.getString(R.string.warning_confirm));
    builder.positiveColor(accentColor);

    builder.onPositive(
        (dialog, which) -> {
          try {
            encryptButtonCallbackInterface.onButtonPressed(intent);
          } catch (Exception e) {
            e.printStackTrace();

            Toast.makeText(
                    main.getActivity(),
                    main.getString(R.string.crypt_encryption_fail),
                    Toast.LENGTH_LONG)
                .show();
          }
        });

    builder.onNegative(
        (dialog, which) -> {
          preferences
              .edit()
              .putBoolean(PreferencesConstants.PREFERENCE_CRYPT_WARNING_REMEMBER, true)
              .apply();
          try {
            encryptButtonCallbackInterface.onButtonPressed(intent);
          } catch (Exception e) {
            e.printStackTrace();

            Toast.makeText(
                    main.getActivity(),
                    main.getString(R.string.crypt_encryption_fail),
                    Toast.LENGTH_LONG)
                .show();
          }
        });

    builder.show();
  }





  public static void setPermissionsDialog(
      final View v,
      View but,
      final HybridFile file,
      final String f,
      final Context context,
      final MainFragment mainFrag) {
    final CheckBox readown = v.findViewById(R.id.creadown);
    final CheckBox readgroup = v.findViewById(R.id.creadgroup);
    final CheckBox readother = v.findViewById(R.id.creadother);
    final CheckBox writeown = v.findViewById(R.id.cwriteown);
    final CheckBox writegroup = v.findViewById(R.id.cwritegroup);
    final CheckBox writeother = v.findViewById(R.id.cwriteother);
    final CheckBox exeown = v.findViewById(R.id.cexeown);
    final CheckBox exegroup = v.findViewById(R.id.cexegroup);
    final CheckBox exeother = v.findViewById(R.id.cexeother);
    String perm = f;
    if (perm.length() < 6) {
      v.setVisibility(View.GONE);
      but.setVisibility(View.GONE);
      Toast.makeText(context, R.string.not_allowed, Toast.LENGTH_SHORT).show();
      return;
    }
    ArrayList<Boolean[]> arrayList = FileUtils.parse(perm);
    Boolean[] read = arrayList.get(0);
    Boolean[] write = arrayList.get(1);
    final Boolean[] exe = arrayList.get(2);
    readown.setChecked(read[0]);
    readgroup.setChecked(read[1]);
    readother.setChecked(read[2]);
    writeown.setChecked(write[0]);
    writegroup.setChecked(write[1]);
    writeother.setChecked(write[2]);
    exeown.setChecked(exe[0]);
    exegroup.setChecked(exe[1]);
    exeother.setChecked(exe[2]);
    but.setOnClickListener(
        v1 -> {
          int perms =
              RootUtils.permissionsToOctalString(
                  readown.isChecked(),
                  writeown.isChecked(),
                  exeown.isChecked(),
                  readgroup.isChecked(),
                  writegroup.isChecked(),
                  exegroup.isChecked(),
                  readother.isChecked(),
                  writeother.isChecked(),
                  exeother.isChecked());

          try {
            RootUtils.changePermissions(
                file.getPath(),
                perms,
                file.isDirectory(context),
                isSuccess -> {
                  if (isSuccess) {
                    Toast.makeText(context, mainFrag.getString(R.string.done), Toast.LENGTH_LONG)
                        .show();
                  } else {
                    Toast.makeText(
                            context,
                            mainFrag.getString(R.string.operation_unsuccesful),
                            Toast.LENGTH_LONG)
                        .show();
                  }
                });
          } catch (ShellNotRunningException e) {
            Toast.makeText(context, mainFrag.getString(R.string.root_failure), Toast.LENGTH_LONG)
                .show();
            e.printStackTrace();
          }
        });
  }

  public static void showChangePathsDialog(
      final MainActivity mainActivity, final SharedPreferences prefs) {
    final MaterialDialog.Builder a = new MaterialDialog.Builder(mainActivity);
    a.input(
        null,
        mainActivity.getCurrentMainFragment().getCurrentPath(),
        false,
        (dialog, charSequence) -> {
          boolean isAccessible = FileUtils.isPathAccessible(charSequence.toString(), prefs);
          dialog.getActionButton(DialogAction.POSITIVE).setEnabled(isAccessible);
        });

    a.alwaysCallInputCallback();

    int accentColor = mainActivity.getAccent();

    a.widgetColor(accentColor);

    a.theme(mainActivity.getAppTheme().getMaterialDialogTheme());
    a.title(R.string.enterpath);

    a.positiveText(R.string.go);
    a.positiveColor(accentColor);

    a.negativeText(R.string.cancel);
    a.negativeColor(accentColor);

    a.onPositive(
        (dialog, which) -> {
          mainActivity
              .getCurrentMainFragment()
              .loadlist(dialog.getInputEditText().getText().toString(), false, OpenMode.UNKNOWN);
        });

    a.show();
  }

  public static MaterialDialog showOtgSafExplanationDialog(ThemedActivity themedActivity) {
    return GeneralDialogCreation.showBasicDialog(
        themedActivity,
        R.string.saf_otg_explanation,
        R.string.otg_access,
        R.string.ok,
        R.string.cancel);
  }
}
