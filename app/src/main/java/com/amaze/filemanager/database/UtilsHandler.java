/*
 * Copyright (C) 2014-2020 Arpit Khurana <arpitkh96@gmail.com>, Vishal Nehra <vishalmeham2@gmail.com>,
 * Emmanuel Messulam<emmanuelbendavid@gmail.com>, Raymond Lai <airwave209gt at gmail.com> and Contributors.
 *
 * This file is part of Amaze File Manager.
 *
 * Amaze File Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.amaze.filemanager.database;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.amaze.filemanager.R;
import com.amaze.filemanager.application.AppConfig;
import com.amaze.filemanager.database.models.OperationData;
import com.amaze.filemanager.database.models.utilities.Bookmark;
import com.amaze.filemanager.database.models.utilities.Grid;
import com.amaze.filemanager.database.models.utilities.Hidden;
import com.amaze.filemanager.database.models.utilities.History;
import com.amaze.filemanager.database.models.utilities.SftpEntry;
import com.amaze.filemanager.database.models.utilities.SmbEntry;
import com.amaze.filemanager.utils.SmbUtil;
import com.googlecode.concurrenttrees.radix.ConcurrentRadixTree;
import com.googlecode.concurrenttrees.radix.node.concrete.DefaultCharArrayNodeFactory;
import com.googlecode.concurrenttrees.radix.node.concrete.voidvalue.VoidValue;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import io.reactivex.schedulers.Schedulers;

/**
 * Created by Vishal on 29-05-2017. Class handles database with tables having list of various
 * utilities like history, hidden files, list paths, grid paths, bookmarks, SMB entry
 *
 * <p>Try to use these functions from a background thread
 */
public class UtilsHandler {

  private static final String TAG = UtilsHandler.class.getSimpleName();

  private final Context context;

  private final UtilitiesDatabase utilitiesDatabase;

  public UtilsHandler(@NonNull Context context, @NonNull UtilitiesDatabase utilitiesDatabase) {
    this.context = context;
    this.utilitiesDatabase = utilitiesDatabase;
  }

  public enum Operation {
    HISTORY,
    HIDDEN,
    LIST,
    GRID,
    BOOKMARKS,

  }

  public void saveToDatabase(OperationData operationData) {
    switch (operationData.type) {
      case HIDDEN:
        utilitiesDatabase
                .hiddenEntryDao()
                .insert(new Hidden(operationData.path))
                .subscribeOn(Schedulers.io())
                .subscribe();
        break;
      case LIST:
        utilitiesDatabase
                .listEntryDao()
                .insert(new com.amaze.filemanager.database.models.utilities.List(operationData.path))
                .subscribeOn(Schedulers.io())
                .subscribe();
        break;
      case GRID:
        utilitiesDatabase
                .gridEntryDao()
                .insert(new Grid(operationData.path))
                .subscribeOn(Schedulers.io())
                .subscribe();
        break;

      default:
        throw new IllegalStateException("Unidentified operation!");
    }
  }

  public void removeFromDatabase(OperationData operationData) {
    switch (operationData.type) {
      case HIDDEN:
        utilitiesDatabase
                .hiddenEntryDao()
                .deleteByPath(operationData.path)
                .subscribeOn(Schedulers.io())
                .subscribe();
        break;

      case LIST:
        utilitiesDatabase
                .listEntryDao()
                .deleteByPath(operationData.path)
                .subscribeOn(Schedulers.io())
                .subscribe();
        break;
      case GRID:
        utilitiesDatabase
                .gridEntryDao()
                .deleteByPath(operationData.path)
                .subscribeOn(Schedulers.io())
                .subscribe();
        break;
      default:
        throw new IllegalStateException("Unidentified operation!");
    }
  }

  public void addCommonBookmarks() {
    File sd = Environment.getExternalStorageDirectory();

    String[] dirs =
            new String[]{
                    new File(sd, Environment.DIRECTORY_DCIM).getAbsolutePath(),
                    new File(sd, Environment.DIRECTORY_DOWNLOADS).getAbsolutePath(),
                    new File(sd, Environment.DIRECTORY_MOVIES).getAbsolutePath(),
                    new File(sd, Environment.DIRECTORY_MUSIC).getAbsolutePath(),
                    new File(sd, Environment.DIRECTORY_PICTURES).getAbsolutePath()
            };

    for (String dir : dirs) {
      saveToDatabase(new OperationData(Operation.BOOKMARKS, new File(dir).getName(), dir));
    }
  }


  public ConcurrentRadixTree<VoidValue> getHiddenFilesConcurrentRadixTree() {
    ConcurrentRadixTree<VoidValue> paths =
            new ConcurrentRadixTree<>(new DefaultCharArrayNodeFactory());

    for (String path :
            utilitiesDatabase.hiddenEntryDao().listPaths().subscribeOn(Schedulers.io()).blockingGet()) {
      paths.put(path, VoidValue.SINGLETON);
    }
    return paths;
  }

  public ArrayList<String> getListViewList() {
    return new ArrayList<>(
            utilitiesDatabase.listEntryDao().listPaths().subscribeOn(Schedulers.io()).blockingGet());
  }

  public ArrayList<String> getGridViewList() {
    return new ArrayList<>(
            utilitiesDatabase.gridEntryDao().listPaths().subscribeOn(Schedulers.io()).blockingGet());
  }

}
