/*
 * Copyright (C) 2014-2020 Arpit Khurana <arpitkh96@gmail.com>, Vishal Nehra <vishalmeham2@gmail.com>,
 * Emmanuel Messulam<emmanuelbendavid@gmail.com>, Raymond Lai <airwave209gt at gmail.com> and Contributors.
 *
 * This file is part of Amaze File Manager.
 *
 * Amaze File Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.amaze.filemanager.ui;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;

import com.amaze.filemanager.R;
import com.amaze.filemanager.adapters.data.LayoutElementParcelable;
import com.amaze.filemanager.asynchronous.services.EncryptService;
import com.amaze.filemanager.filesystem.HybridFileParcelable;
import com.amaze.filemanager.filesystem.PasteHelper;
import com.amaze.filemanager.filesystem.files.EncryptDecryptUtils;
import com.amaze.filemanager.filesystem.files.FileUtils;
import com.amaze.filemanager.ui.activities.MainActivity;
import com.amaze.filemanager.ui.activities.superclasses.ThemedActivity;
import com.amaze.filemanager.ui.dialogs.GeneralDialogCreation;
import com.amaze.filemanager.ui.fragments.MainFragment;
import com.amaze.filemanager.ui.fragments.preference_fragments.PreferencesConstants;
import com.amaze.filemanager.ui.provider.UtilitiesProvider;
import com.amaze.filemanager.utils.DataUtils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

/**
 * This class contains the functionality of the PopupMenu for each file in the MainFragment
 *
 * @author Emmanuel on 25/5/2017, at 16:39. Edited by bowiechen on 2019-10-19.
 */
public class ItemPopupMenu extends PopupMenu implements PopupMenu.OnMenuItemClickListener {

  private final Context context;
  private final MainActivity mainActivity;
  private final UtilitiesProvider utilitiesProvider;
  private final MainFragment mainFragment;
  private final SharedPreferences sharedPrefs;
  private final LayoutElementParcelable rowItem;
  private final int accentColor;

  public ItemPopupMenu(
      Context c,
      MainActivity ma,
      UtilitiesProvider up,
      MainFragment mainFragment,
      LayoutElementParcelable ri,
      View anchor,
      SharedPreferences sharedPreferences) {
    super(c, anchor);

    context = c;
    mainActivity = ma;
    utilitiesProvider = up;
    this.mainFragment = mainFragment;
    sharedPrefs = sharedPreferences;
    rowItem = ri;
    accentColor = mainActivity.getAccent();

    setOnMenuItemClickListener(this);
  }

  @Override
  public boolean onMenuItemClick(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.about:
        GeneralDialogCreation.showPropertiesDialogWithPermissions(
            (rowItem).generateBaseFile(),
            rowItem.permissions,
            (ThemedActivity) mainFragment.getActivity(),
            mainActivity.isRootExplorer(),
            utilitiesProvider.getAppTheme());
        return true;
      case R.id.delete:
        ArrayList<LayoutElementParcelable> positions = new ArrayList<>();
        positions.add(rowItem);
        GeneralDialogCreation.deleteFilesDialog(
            context,
            mainFragment.getElementsList(),
            mainFragment.getMainActivity(),
            positions,
            utilitiesProvider.getAppTheme());
        return true;
      case R.id.return_select:
        mainFragment.returnIntentResults(rowItem.generateBaseFile());
        return true;
    }
    return false;
  }
}
